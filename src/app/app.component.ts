import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import firebase from 'firebase';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public rootPage: any;
  zone: NgZone;

  constructor(platform: Platform) {


   var config = {
    apiKey: "AIzaSyD-VVh_xjKsazPbORA4N4W2Owy1wUpsN04",
    authDomain: "fireblogger-19707.firebaseapp.com",
    databaseURL: "https://fireblogger-19707.firebaseio.com",
    storageBucket: "fireblogger-19707.appspot.com",
    messagingSenderId: "506832021303"
  };
  firebase.initializeApp(config);

  this.zone = new NgZone({});
  const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
    this.zone.run( () => {
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else { 
        this.rootPage = HomePage; 
        unsubscribe();
      }
    });     
  });

  platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
}
}
