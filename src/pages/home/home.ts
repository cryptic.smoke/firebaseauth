import { Component } from '@angular/core';
import {AuthData} from '../../providers/auth-data';
import {LoginPage} from '../login/login';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [AuthData]
})
export class HomePage {

  constructor(public navCtrl: NavController, public authData: AuthData) {
    
  }

  logOut(){
  this.authData.logoutUser().then(() => {
    this.navCtrl.setRoot(LoginPage);
  });
}

}
